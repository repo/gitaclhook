# This file is part of gitaclhook -*- perl -*-
# Copyright (C) 2013 Sergey Poznyakoff <gray@gnu.org>
#
# Gitaclhook is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Gitaclhook is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with gitaclhook.  If not, see <http://www.gnu.org/licenses/>.

use ExtUtils::MakeMaker;

my %pm = ('lib/GitACL.pm' => '$(INST_LIBDIR)/GitACL.pm',
          'lib/GitACL/File.pm' => '$(INST_LIBDIR)/GitACL/File.pm',
          'lib/GitACL/LDAP.pm' => '$(INST_LIBDIR)/GitACL/LDAP.pm');

WriteMakefile(
	'NAME'              => 'gitaclhook',
        'ABSTRACT_FROM'     => 'gitaclhook',
        'AUTHOR'            => 'Sergey Poznyakoff <gray@gnu.org>',
        'LICENSE'           => 'gpl',
	'FIRST_MAKEFILE'    => 'Makefile',
	'VERSION'           => '1.01',
	'PM'                => \%pm,
	'EXE_FILES'         => [ 'gitaclhook' ],
	'PREREQ_PM'         => { 'Getopt::Long' => 2.34,
                                 'File::Spec' => 3.39,
				 'Net::CIDR' => 0.17 }
);


